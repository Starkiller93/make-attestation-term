package com.gdn.make_attestation.enums;

public enum FieldsEnum {
    NAME("Champ de texte 1.Page 1", "STRING", "name"),
    BIRTHDATE("Champ de texte 2.Page 1", "DATE", "birthdate"),
    ADDRESS("Champ de texte 3.Page 1", "STRING", "address"),
    BUSINESS_TRIP_CHECKBOX("Case à cocher 1.Page 1", "CHECKBOX", "businessTrip"),
    LIVING_TRIP_CHECKBOX("Case à cocher 2.Page 1", "CHECKBOX", "livingTrip"),
    HEALTH_TRIP_CHECKBOX("Case à cocher 3.Page 1", "CHECKBOX", "healthTrip"),
    FAMILY_TRIP_CHECKBOX("Case à cocher 4.Page 1", "CHECKBOX", "familyTrip"),
    SPORT_TRIP_CHECKBOX("Case à cocher 5.Page 1", "CHECKBOX", "sportTrip"),
    COUNTRY("Champ de texte 4.Page 1", "STRING", "country"),
    DAY("Champ de texte 5.Page 1", "INTEGER", "day"),
    MONTH("Champ de texte 7.Page 1", "INTEGER", "month");

    private String key;

    private String fieldType;

    private String fieldName;
    
    FieldsEnum(String key, String fieldType, String fieldName) {
        this.key = key;
        this.fieldType = fieldType;
        this.fieldName = fieldName;
    }

    public String getKey() {
        return this.key;
    }

    public String getFieldType() {
        return this.fieldType;
    }

    public String getFieldName() {
        return this.fieldName;
    }

    public String toString()  {
        return key;
    }
}