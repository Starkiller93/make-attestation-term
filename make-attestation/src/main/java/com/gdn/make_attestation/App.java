package com.gdn.make_attestation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Scanner;

import com.gdn.make_attestation.domain.UserEntries;
import com.gdn.make_attestation.utils.MakeAttestationUtils;

public class App {
   public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);
      boolean isOK = false;
      UserEntries userEntries = new UserEntries();

      //Valeurs remplie automatiquement à la date du jour
      LocalDate currentDate = LocalDate.now();
      userEntries.setDay(currentDate.getDayOfMonth());
      userEntries.setMonth(currentDate.getMonthValue());

      System.out.println("Make Attestation");
      System.out.println();
      System.out.println(
            "Utilisation : Permet de remplir le template officiel de l'attesation de sortie à partir des informations que vous allez rentrer.");
      System.out.println();
      System.out.println();

      System.out.print("Nom/Prenom : ");
      userEntries.setName(scanner.nextLine());
      System.out.println();
      
      while(!isOK) {
         isOK = true;
         System.out.print("Date de naissance (dd/mm/yyyy) : ");
         String dateToString = scanner.nextLine();
         SimpleDateFormat format = new SimpleDateFormat("dd/mm/yyyy");
         Date date = new Date();
         try {
            date = format.parse(dateToString);
            userEntries.setBirthdate(date);
         } catch (ParseException e) {
            System.out.println("Format de la date de naissance incorrect");
            isOK = false;
         }
      }
      System.out.println();

      System.out.print("Adresse : ");
      userEntries.setAddress(scanner.nextLine());
      System.out.println();

      System.out.println("Raisons :");
      System.out.println("    1. Déplacement professionnel");
      System.out.println("    2. Déplacement pour faire les courses");
      System.out.println("    3. Déplacement pour raison de santé");
      System.out.println("    4. Déplacement pour raison familliale");
      System.out.println("    5. Déplacement pour activité physique");
      
      isOK = false;
      while(!isOK) {
         isOK = true;
         System.out.print("Votre choix (1-5) : ");
         int choice = scanner.nextInt();
         switch (choice) {
         case 1:
            userEntries.setBusinessTrip(true);
            break;
         case 2:
            userEntries.setLivingTrip(true);
            break;
         case 3:
            userEntries.setHealtTrip(true);
            break;
         case 4:
            userEntries.setFamilyTrip(true);
            break;
         case 5:
            userEntries.setSportTrip(true);
            break;
         default:
            System.out.print("Le choix indiqué n'est pas correct");
            isOK = false;
            break;
         }
         System.out.println();
      }

      System.out.print("Ville où est réalisé l'attestation : ");
      userEntries.setCountry(scanner.next());
      System.out.println();

      scanner.close();

      String filename = MakeAttestationUtils.fillAttestation(userEntries);
      
      System.out.println("Votre attestation a été généré avec succès dans votre dossier home: " + filename);
   }
}
