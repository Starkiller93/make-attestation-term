package com.gdn.make_attestation.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import com.gdn.make_attestation.enums.FieldsEnum;
import com.itextpdf.forms.PdfAcroForm;
import com.itextpdf.forms.fields.PdfFormField;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;

import com.gdn.make_attestation.domain.UserEntries;

public class MakeAttestationUtils {
    public static String fillAttestation(UserEntries userEntries) {
        String fileName = "attestation-" + UUID.randomUUID().toString() + ".pdf";
        try {
            File outputFile = new File(System.getProperty("user.home") + "/" + fileName);
            // Ouverture du document
            InputStream inputStream = MakeAttestationUtils.class.getClassLoader().getResourceAsStream("attestation_template.pdf");
           // FileInputStream inputStream = new FileInputStream();
            FileOutputStream outputStream = new FileOutputStream(outputFile);
            PdfReader reader = new PdfReader(inputStream);
            PdfWriter writer = new PdfWriter(outputStream);
            PdfDocument pdfDoc = new PdfDocument(reader, writer);

            // Mapping des champs du PDF
            PdfAcroForm form = PdfAcroForm.getAcroForm(pdfDoc, true);
            Map<String, PdfFormField> fields = form.getFormFields();

            // Application des valeurs de l'utilisateur
            for (FieldsEnum field : FieldsEnum.values()) {
                switch (field.getFieldType()) {
                case "STRING":
                    String stringValue = (String)userEntries.getAttributes().get(field.getFieldName());
                    fields.get(field.getKey()).setValue(stringValue);
                    break;
                case "DATE":
                    Date dateValue = (Date)userEntries.getAttributes().get(field.getFieldName());
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
                    fields.get(field.getKey()).setValue(formatter.format(dateValue));
                    break;
                case "INTEGER":
                    int integerValue = (int)userEntries.getAttributes().get(field.getFieldName());
                    fields.get(field.getKey()).setValue(formatOn2Digits(integerValue));
                    break;
                case "CHECKBOX":
                    boolean booleanValue = (boolean)userEntries.getAttributes().get(field.getFieldName());
                    if(booleanValue) {
                        fields.get(field.getKey()).setValue("YES");
                    }
                    break;
                }
            }
            // Fermeture du flux
            pdfDoc.close();
        } catch (FileNotFoundException e) {
            System.out.println("Fichier non trouvé");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Une erreur est survenu lors de la lecture/ecriture du PDF");
            e.printStackTrace();
        } catch(Exception e) {
            System.out.println("Une erreur est survenu");
            e.printStackTrace();
        }
        return fileName;
    }

    public static String formatOn2Digits(int value) {
        if(value <= 9) {
            return  "0" + String.valueOf(value);
        }
        return String.valueOf(value);
    }
}
