package com.gdn.make_attestation.domain;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

//Ajouter le path ?

public class UserEntries {
    private String name;

    private Date birthdate;

    private String address;

    private boolean businessTrip;

    private boolean livingTrip;

    private boolean healthTrip;

    private boolean familyTrip;

    private boolean sportTrip;

    private String country;

    private int day;

    private int month;

    public UserEntries() {
        this.businessTrip = false;
        this.livingTrip = false;
        this.healthTrip = false;
        this.familyTrip = false;
        this.sportTrip = false;
    }

    public String getName() {
        return this.name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public void setBusinessTrip(boolean businessTrip) {
        this.businessTrip = businessTrip;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setFamilyTrip(boolean familyTrip) {
        this.familyTrip = familyTrip;
    }

    public void setHealtTrip(boolean healthTrip) {
        this.healthTrip = healthTrip;
    }

    public void setLivingTrip(boolean livingTrip) {
        this.livingTrip = livingTrip;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSportTrip(boolean sportTrip) {
        this.sportTrip = sportTrip;
    }

    public Map<String, Object> getAttributes() {
        Map<String, Object> attributesMap = new HashMap<>();
        attributesMap.put("name", this.name);
        attributesMap.put("birthdate", this.birthdate);
        attributesMap.put("address", this.address);
        attributesMap.put("businessTrip", this.businessTrip);
        attributesMap.put("livingTrip", this.livingTrip);
        attributesMap.put("healthTrip", this.healthTrip);
        attributesMap.put("familyTrip", this.familyTrip);
        attributesMap.put("sportTrip", this.sportTrip);
        attributesMap.put("country", this.country);
        attributesMap.put("day", this.day);
        attributesMap.put("month", this.month);
        return attributesMap;
    }
}