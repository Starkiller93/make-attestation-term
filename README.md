# Make-Attestation (TERM)

This project allow to generate an exit attestion (Coronavirus) with your personnal informations. The only 2 things you must to do are print and sign the document ! :)

## Getting started

This project is easy to deploy and use. You just need JAVA and Maven.

### Prerequisites

- [JAVA 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)

- [MAVEN](https://maven.apache.org/download.cgi)

### Launch the project

- Compile the source
  
```bash
mvn clean compile
```

- Build the jar with it dependencies

```bash
mvn assembly:single
```

## Description

This version of the project used a simple terminal to communicate with the user and generate the pdf with his own informations in his Home's directory.
